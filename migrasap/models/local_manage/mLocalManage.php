<?php
class MLocalManage{
	private $conn_m;
	private $conn_h;

	function MLocalManage(){
		include_once 'conex.php';
		$this->conn_m = new Conex("msmirror");
		$this->conn_h = new Conex("mshandling");
	}

	function close_conex(){
		$this->conn_m->close_conex();
		$this->conn_h->close_conex();
	}

	function select_0(){
		$sql = "SELECT * FROM sap_data;";
        #echo $sql;
        return $this->conn_m->run($sql,'sel');
	}

	function select_1(){
		$sql = "SELECT * FROM lsmw_data;";
        #echo $sql;
        return $this->conn_h->run($sql,'sel');
	}

	function select_2($ist){
		$sql = "SELECT * FROM sap_fields WHERE id_sap_table = $ist;";
        #echo $sql;
        return $this->conn_m->run($sql,'sel');
	}

	function select_3(){
		$sql = "SELECT * FROM mapping;";
        #echo $sql;
        return $this->conn_h->run($sql,'sel');
	}

	function insert_0($isf, $v){
		$sql = "INSERT INTO sap_data VALUES(NULL, $isf, \"$v\");";
        #echo $sql;
        return $this->conn_m->run($sql,'ins');
	}

	function insert_1($ilf, $v){
		$sql = "INSERT INTO lsmw_data VALUES(NULL, $ilf, \"$v\");";
        #echo $sql;
        return $this->conn_h->run($sql,'ins');
	}

	/*function update($i, $n, $h, $s, $c, $u, $l, $r, $f){
		$sql = "UPDATE connections SET name = '$n', host = '$h', sysnr = '$s', client = '$c', user = '$u', lang = '$l', saprouter = '$r', function = '$f' WHERE id = $i";
        #echo $sql;
        return $this->conn->run($sql,'upd');
	}

	function delete($i){
		$sql = "DELETE FROM connections WHERE id = $i";
        #echo $sql;
        return $this->conn->run($sql,'del');
	}*/
}
?>