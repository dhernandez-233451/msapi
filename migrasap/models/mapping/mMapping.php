<?php
class MMapping{
	private $conn_m;
	private $conn_h;

	function MMapping(){
		include_once 'conex.php';
		$this->conn_m = new Conex("msmirror");
		$this->conn_h = new Conex("mshandling");
	}

	function close_conex(){
		$this->conn_m->close_conex();
		$this->conn_h->close_conex();
	}

	function select_0($il){
		$sql = "SELECT lsmws.id AS 'id_lsmw', lsmws.title AS 'lsmw', lsmw_fields.id AS 'id_lsmw_field', lsmw_fields.field AS 'lsmw_field', mapping.id_lsmw_field AS 'mapping_lsmw', mapping.id_sap_field AS 'mapping_sap' FROM lsmw_fields INNER JOIN lsmws ON lsmw_fields.id_lsmw = lsmws.id LEFT JOIN mapping ON lsmw_fields.id = mapping.id_lsmw_field WHERE lsmw_fields.id_lsmw = $il ORDER BY lsmw_fields.id;";
        #echo $sql;
        return $this->conn_h->run($sql,'sel');
	}

	function select_1(){
		$sql = "SELECT sap_tables.id AS 'id_sap_table', sap_tables.name AS 'sap_table', sap_fields.id AS 'id_sap_field', sap_fields.name AS 'sap_field' FROM sap_fields INNER JOIN sap_tables ON sap_fields.id_sap_table = sap_tables.id ;";
        #echo $sql;
        return $this->conn_m->run($sql,'sel');
	}

	function insert($ilf, $isf){
		$sql = "INSERT INTO mapping VALUES($ilf, $isf);";
        #echo $sql;
        return $this->conn_h->run($sql,'ins');
	}

	/*function update($i, $f){
		$sql = "UPDATE lsmw_fields SET field = '$f' WHERE id = $i";
        #echo $sql;
        return $this->conn->run($sql,'upd');
	}*/

	function delete($i){
		$sql = "DELETE FROM mapping WHERE id_lsmw_field = $i;";
        #echo $sql;
        return $this->conn_h->run($sql,'del');
	}
}
?>