<?php
class MRules{
	private $conn_s;
	private $conn_h;

	function MRules(){
		include_once 'conex.php';
		$this->conn_s = new Conex("mssystem");
		$this->conn_h = new Conex("mshandling");
	}

	function close_conex(){
		$this->conn_s->close_conex();
		$this->conn_h->close_conex();		
	}

	/*function select($ilf){
		$sql = "SELECT $ilf AS 'id_lsmw_field', rules.id AS 'id_rule', rules.name AS 'rule', IF(t1.id_lsmw_field != '', 'true', 'false') AS 'active' FROM rules LEFT JOIN (SELECT * FROM rules_lsmw WHERE rules_lsmw.id_lsmw_field = $ilf) AS t1 ON rules.id = t1.id_rule ORDER BY rules.id;";
        #echo $sql;
        return $this->conn->run($sql,'sel');
	}*/

	function select_0(){
		$sql = "SELECT * FROM rules;";
        #echo $sql;
        return $this->conn_s->run($sql,'sel');
	}

	function select_1($ilf){
		$sql = "SELECT * FROM rules_lsmw WHERE id_lsmw_field = $ilf ORDER BY id_rule;";
        #echo $sql;
        return $this->conn_h->run($sql,'sel');
	}

	function insert($ir, $ilf){
		$sql = "INSERT INTO rules_lsmw VALUES($ir, $ilf);";
        #echo $sql;
        return $this->conn_h->run($sql,'ins');
	}

	/*function update($i, $f){
		$sql = "UPDATE lsmw_fields SET field = '$f' WHERE id = $i";
        #echo $sql;
        return $this->conn->run($sql,'upd');
	}*/

	function delete($i){
		$sql = "DELETE FROM rules_lsmw WHERE id_lsmw_field = $i;";
        #echo $sql;
        return $this->conn_h->run($sql,'del');
	}
}
?>