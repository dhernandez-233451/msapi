<?php
class MSapField{
	private $conn;

	function MSapField(){
		include_once 'conex.php';
		$this->conn = new Conex("msmirror");
	}

	function close_conex(){
		$this->conn->close_conex();
	}

	function select($ist, $n){
		$sql = "SELECT sap_fields.id, sap_tables.id AS 'id_sap_table', sap_tables.name AS 'sap_table', sap_fields.name FROM sap_fields INNER JOIN sap_tables ON sap_fields.id_sap_table = sap_tables.id WHERE sap_fields.id_sap_table = $ist;";
        #echo $sql;
        return $this->conn->run($sql,'sel');
	}

	function insert($ist, $n){
		$sql = "INSERT INTO sap_fields VALUES(NULL, $ist, \"$n\");";
        #echo $sql;
        return $this->conn->run($sql,'ins');
	}

	function update($i, $n){
		$sql = "UPDATE sap_fields SET name = \"$n\" WHERE id = $i";
        #echo $sql;
        return $this->conn->run($sql,'upd');
	}

	function delete($i){
		$sql = "DELETE FROM sap_fields WHERE id = $i";
        #echo $sql;
        return $this->conn->run($sql,'del');
	}
}
?>