<?php
class MUploadLayout{
	private $conn_h;
	private $conn_s;	

	function MUploadLayout(){
		include_once 'conex.php';
		$this->conn_h = new Conex("mshandling");
		$this->conn_s = new Conex("mssystem");
	}

	function close_conex(){
		$this->conn_h->close_conex();
		$this->conn_s->close_conex();
	}

	function select_0($il){
		$sql = "SELECT * FROM lsmw_fields WHERE lsmw_fields.id_lsmw = $il;";
        #echo $sql;
        return $this->conn_h->run($sql,'sel');
	}

	/*function select_1($il){
		$sql = "SELECT lsmw_fields.id AS 'id_field', lsmw_fields.field, rules.id AS 'id_rule', rules.name AS 'rule' FROM rules_lsmw INNER JOIN rules ON rules_lsmw.id_rule = rules.id RIGHT JOIN lsmw_fields ON rules_lsmw.id_lsmw_field = lsmw_fields.id WHERE lsmw_fields.id_lsmw = $il;";
        #echo $sql;
        return $this->conn->run($sql,'sel');
	}*/

	function select_1(){
		$sql = "SELECT * FROM rules;";
        #echo $sql;
        return $this->conn_s->run($sql,'sel');
	}

	function select_2($il){
		$sql = "SELECT lsmw_fields.id AS 'id_field', lsmw_fields.field, rules_lsmw.id_rule FROM rules_lsmw RIGHT JOIN lsmw_fields ON rules_lsmw.id_lsmw_field = lsmw_fields.id WHERE lsmw_fields.id_lsmw = $il;";
        #echo $sql;
        return $this->conn_h->run($sql,'sel');
	}

	function insert($if, $v){
		$sql = "INSERT INTO lsmw_data VALUES (NULL, $if, \"$v\");";
        #echo $sql;
        return $this->conn_h->run($sql,'ins');
	}

	/*function update($i, $f){
		$sql = "UPDATE SET WHERE ;";
        #echo $sql;
        return $this->conn->run($sql,'upd');
	}*/

	function delete($il){
		$sql = "DELETE FROM lsmw_data WHERE lsmw_data.id_lsmw_fields IN (SELECT lsmw_fields.id FROM lsmw_fields WHERE lsmw_fields.id_lsmw = $il);";
        #echo $sql;
        return $this->conn_h->run($sql,'del');
	}
}
?>