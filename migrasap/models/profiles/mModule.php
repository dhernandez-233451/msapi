<?php
class MModule{
	private $conn;

	function MModule(){
		include_once 'conex.php';
		$this->conn = new Conex("mssystem");
	}

	function close_conex(){
		$this->conn->close_conex();
	}

	function select($i){
		$sql = "SELECT modules.id, modules.name, IF(vt.profile != '', TRUE, FALSE) AS 'active' FROM modules LEFT JOIN (SELECT profiles.name AS 'profile', permits.id_module, modules.name AS 'module' FROM profiles INNER JOIN permits ON profiles.id = permits.id_profile INNER JOIN modules ON permits.id_module = modules.id WHERE profiles.id = $i) AS vt ON modules.id = vt.id_module;";
        #echo $sql;
        return $this->conn->run($sql,'sel');
	}

	function insert($p, $m){
		$sql = "INSERT INTO permits VALUES($p, $m);";
        #echo $sql;
        return $this->conn->run($sql,'ins');
	}

	function update(){
		$sql = "";
        #echo $sql;
        return $this->conn->run($sql,'upd');
	}

	function delete($i){
		$sql = "DELETE FROM permits WHERE id_profile = $i";
        #echo $sql;
        return $this->conn->run($sql,'del');
	}
}
?>