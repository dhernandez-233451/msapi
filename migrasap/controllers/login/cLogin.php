<?php
class CLogin{
	private $mlogin;
	private $user;
	private $passwd;

	function CLogin($data){
		include_once '../models/login/mLogin.php';
		$this->mlogin = new MLogin();
		$this->user = $data->login->user;
		$this->passwd = $data->login->passwd;
	}

	function control($action){
		switch ($action) {
			case 'validate':
				$this->validate();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0002", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function validate(){
		$r  = $this->mlogin->getUser($this->user, $this->passwd);

		if( $r['error'] == NULL){
			if($r['res'] != NULL){
				$result->id = $r['res'][0]->id;
				$result->user = $r['res'][0]->user;
				$result->name  = $r['res'][0]->name;
				$result->profile = $r['res'][0]->profile;				
				
				$r2 = $this->mlogin->getModules($r['res'][0]->id_profile);
				foreach ($r2['res'] as $d) $result->permits[] = $d->id_module;

				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => (object)['login' => $result]]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0004", 'message' => "No se encontraron datos ".$this->user]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0003", 'message' => $r['error']]]);
		
		$this->mlogin->close_conex();
	}
}
?>