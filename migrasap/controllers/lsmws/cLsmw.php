<?php
class CLsmw{
	private $mlsmw;
	private $id;
	private $title;

	function CLsmw($data){
		include_once '../models/lsmws/mLsmw.php';
		$this->mlsmw = new MLsmw();

		$this->id = $data->lsmw->id;
		$this->title = $data->lsmw->title;
	}

	function control($action){
		switch ($action) {
			case 'find':
				$this->find();
				break;
			case 'new':
				$this->new();
				break;
			case 'edit':
				$this->edit();
				break;
			case 'delete':
				$this->delete();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0044", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function find(){
		$r  = $this->mlsmw->select($this->id, $this->title);

		if( $r['error'] == NULL){
			if($r['res'] != NULL){
				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => (object)['lsmws' => $r['res']]]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0046", 'message' => "No se encontraron datos ".$this->name]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0045", 'message' => $r['error']]]);

		$this->mlsmw->close_conex();
	}

	function new(){
		$r  = $this->mlsmw->insert($this->title);
		$this->mlsmw->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0047", 'message' => $r['error']]]);
	}

	function edit(){
		$r  = $this->mlsmw->update($this->id, $this->title);
		$this->mlsmw->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0048", 'message' => $r['error']]]);
	}

	function delete(){
		$r  = $this->mlsmw->delete($this->id);
		$this->mlsmw->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0049", 'message' => $r['error']]]);
	}
}
?>