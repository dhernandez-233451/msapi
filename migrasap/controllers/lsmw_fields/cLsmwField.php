<?php
class CLsmwField{
	private $smlsmwfield;
	private $id;
	private $id_lsmw;
	private $field;

	function CLsmwField($data){
		include_once '../models/lsmw_fields/mLsmwField.php';
		$this->mlsmwfield = new MLsmwField();

		$this->id = $data->lsmwField->id_lsmw;
		$this->id_lsmw = $data->lsmwField->id_lsmw;
		$this->field = $data->lsmwField->field;
	}

	function control($action){
		switch ($action) {
			case 'find':
				$this->find();
				break;
			case 'new':
				$this->new();
				break;
			case 'edit':
				$this->edit();
				break;
			case 'delete':
				$this->delete();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0038", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function find(){
		$r  = $this->mlsmwfield->select($this->id_lsmw, $this->field);

		if( $r['error'] == NULL){
			if($r['res'] != NULL){
				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => (object)['lsmwFields' => $r['res']]]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0040", 'message' => "No se encontraron datos ".$this->name]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0039", 'message' => $r['error']]]);

		$this->mlsmwfield->close_conex();
	}

	function new(){
		$r  = $this->mlsmwfield->insert($this->id_lsmw, $this->field);
		$this->mlsmwfield->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0041", 'message' => $r['error']]]);
	}

	function edit(){
		$r  = $this->mlsmwfield->update($this->id, $this->field);
		$this->mlsmwfield->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0042", 'message' => $r['error']]]);
	}

	function delete(){
		$r  = $this->mlsmwfield->delete($this->id);
		$this->mlsmwfield->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0043", 'message' => $r['error']]]);
	}
}
?>