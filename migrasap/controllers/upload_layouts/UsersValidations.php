<?php
include_once 'upload_layouts/UsersCorrection.php';
class UsersValidations extends UsersCorrection{
	public $results;
	private $data;
	private $xdata;
	private $columns;
	private $rules;	

	function UsersValidations($data, $columns, $rules){
		$this->data = $data;
		$this->xdata = $data;
		$this->columns = $columns;
		$this->rules = $rules;
		#$this->validate();
		$this->process();
	}

	function validate($r, $v){
		$res = NULL;
		switch ($r) {
			case 1:
				$res = $this->capitalLetters($v);
				break;
			case 2:
				$res = $this->lowercase($v);
				break;
			case 3:
				$res = $this->statementMode($v);
				break;
			case 4:
				$res = $this->capLetAtBegWord($v);
				break;
			case 5:
				$res = $this->emptyCell($v);
				break;
			case 6:
				$res = $this->onlyLetters($v);
				break;
			case 7:
				$res = $this->onlyNumbers($v);
				break;
			case 8:
				$res = $this->noAccents($v);
				break;
			
			default:
				# code...
				break;
		}
		return $res;
	}

	function addResult($s, $if, $f, $rw, $v, $ir, $rl, $c){
		$this->results[] = (object)[
			'severity' => $s,
			'id_field' => $if,
			'field' => $f,
			'row' => $rw,
			'value' => $v,
			'id_rule' => $ir,
			'rule' => $rl,
			'comment' => $c
		];
	}

	function process(){
		foreach($this->rules as $r) foreach($this->columns as $c) if($r->id_field == $c->id) for($i = 0; @$this->data[$c->field][$i]; $i++){
						$res = $this->validate($r->id_rule, $this->data[$c->field][$i]);
						if($res != NULL) {
							$this->xdata[$c->field][$i] = $res['correction'];
							$this->addResult($res['severity'], $c->id, $c->field, $i + 1, $this->data[$c->field][$i], $r->id_rule, $r->rule, $res['comment']);
						}
					} 
	}

	function getChangedValues(){
		return $this->xdata;
	}

	/*function removeSymbols($v){
		return $v
	}*/

	function capitalLetters($v){
		$res = NULL;
		$r = "";
		$abc = "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ .,'1234567890";

		for($i = 0; $i<strlen($v); $i++){
			$e = false;
			for($j = 0; $j<strlen($abc); $j++)if($v[$i] == $abc[$j]){
				$e = true;
				break;
			}
			if(!$e)$r .= "Falló en: ".$v[$i].". ";
		}
		if($r != ""){
			$r = substr($r, 0, -2);
			$res['severity'] = "L";
			$res['correction'] = $this->getCapitalLetters($v);
			$res['comment'] = "Se propone: ".$res['correction'].". ".$r;
		}
		
		return $res;
	}

	function lowercase($v){
		$res = NULL;
		$r = "";
		$abc = "aábcdeéfghiíjklmnñoópqrstuúüvwxyz .,'1234567890";

		for($i = 0; $i<strlen($v); $i++){
			$e = false;
			for($j = 0; $j<strlen($abc); $j++)if($v[$i] == $abc[$j]){
				$e = true;
				break;
			}
			if(!$e)$r .= "Falló en: ".$v[$i].". ";
		}
		if($r != ""){
			$r = substr($r, 0, -2);
			$res['severity'] = "L";
			$res['correction'] = $this->getLowercase($v);
			$res['comment'] = "Se propone: ".$res['correction'].". ".$r;
		}

		return $res;
	}

	function statementMode($v){
		$res = NULL;
		$r = "";
		$abc1 = "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ";
		$abc2 = "aábcdeéfghiíjklmnñoópqrstuúüvwxyz .,'1234567890";

		$e = false;
		for($j = 0; $j<strlen($abc1); $j++)if($v[0] == $abc1[$j]){
				$e = true;
				break;
			}
		if(!$e)$r .= "Falló en: ".$v[0].". ";
		
		for($i = 1; $i<strlen($v); $i++){
			$e = false;
			for($j = 0; $j<strlen($abc2); $j++)if($v[$i] == $abc2[$j]){
				$e = true;
				break;
			}
			if(!$e)$r .= "Falló en: ".$v[$i].". ";
		}

		if($r != ""){
			$r = substr($r, 0, -2);
			$res['severity'] = "L";
			$res['correction'] = $this->getStatementMode($v);
			$res['comment'] = "Se propone: ".$res['correction'].". ".$r;
		}

		return $res;
	}

	//Capital letter at the beginning of each word
	function capLetAtBegWord($v){
		$res = NULL;
		$r = "";
		$abc1 = "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ";
		$abc2 = "aábcdeéfghiíjklmnñoópqrstuúüvwxyz.,'1234567890";

		$words = explode(" ", $v);
		foreach ($words as $w) {
			$e = false;
			for($j = 0; $j<strlen($abc1); $j++)if($w[0] == $abc1[$j]){
					$e = true;
					break;
				}
			if(!$e)$r .= "Falló en: ".$w[0].". ";

			for ($i=0; $i <strlen($w) ; $i++) {
				$e = false;
				for($j = 0; $j<strlen($abc2); $j++)if($w[$i] == $abc2[$j]){
					$e = true;
					break;
				}
				if(!$e)$r .= "Falló en: ".$w[$i].". ";
			}
		}

		if($r != ""){
			$r = substr($r, 0, -2);
			$res['severity'] = "L";
			$res['correction'] = $this->getCapLetAtBegWord($v);
			$res['comment'] = "Se propone: ".$res['correction'].". ".$r;
		}

		return $res;
	}

	function emptyCell($v){
		$res = NULL;
		if($v == "" || $v == " "){
			$r = "No debería habar valores vacíos";
			$res['severity'] = "L";
			$res['correction'] = $this->getEmptyCell($v);
			$res['comment'] = "Dejar como: ".$res['correction'].". ".$r;
		}
		return $res;
	}

	function onlyLetters($v){
		$res = NULL;
		$r = "";
		$abc = "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZaábcdeéfghiíjklmnñoópqrstuúüvwxyz .,'";
		#if(!preg_match('/^[a-zA-Z\s]+$/', $v){}

		for($i = 0; $i<strlen($v); $i++){
			$e = false;
			for($j = 0; $j<strlen($abc); $j++)if($v[$i] == $abc[$j]){
				$e = true;
				break;
			}
			if(!$e)$r .= "Falló en: ".$v[$i].". ";
		}

		if($r != ""){
			$r = substr($r, 0, -2);
			$res['severity'] = "L";
			$res['correction'] = $this->getOnlyLetters($v);
			$res['comment'] = "Se propone: ".$res['correction'].". ".$r;
		}
		return $res;
	}

	function onlyNumbers($v){
		$res = NULL;
		$r = "";
		$abc = "1234567890.,-";

		for($i = 0; $i<strlen($v); $i++){
			$e = false;
			for($j = 0; $j<strlen($abc); $j++)if($v[$i] == $abc[$j]){
				$e = true;
				break;
			}
			if(!$e)$r .= "Falló en: ".$v[$i].". ";
		}

		if($r != ""){
			$r = substr($r, 0, -2);
			$res['severity'] = "L";
			$res['correction'] = $this->getOnlyNumbers($v);
			$res['comment'] = "Se propone: ".$res['correction'].". ".$r;
		}
		return $res;
	}

	function noAccents($v){
		$res = NULL;
		$r = "";
		$abc = "ÁÉÍÓÚÜáéíóúü";

		for($i = 0; $i<strlen($v); $i++) for($j = 0; $j<strlen($abc); $j++) if($v[$i] == $abc[$j]){
					$r .= "Falló en: ".$v[$i].". ";
					break;	
				}

		if($r != ""){
			$r = substr($r, 0, -2);
			$res['severity'] = "L";
			$res['correction'] = $this->getNoAccents($v);
			$res['comment'] = "Se propone: ".$res['correction'].". ".$r;
		}
		return $res;
	}

	#addResult($s, $if, $f, $rw, $v, $ir, $rl, $c)
}
?>