<?php
class SystemValidations{
	public $results;
	private $data;
	private $columns;

	function SystemValidations($data, $columns){		
		$this->data = $data;
		$this->columns = $columns;		
		$this->validate();
	}

	function validate(){
		$this->numColumns();
		$this->missingColumns();
		$this->leftoverColumns();
		$this->emptyRows();
		$this->formulasFunctions();
	}

	function addResult($if, $f, $rw, $ir, $rl, $c){
		$this->results[] = (object)[
			'severity' => "H",
			'id_field' => $if,
			'field' => $f,
			'row' => $rw,
			'value' => "",
			'id_rule' => $ir,
			'rule' => $rl,
			'comment' => $c
		];
	}

	function numColumns(){
		if(count($this->data) != count($this->columns)) $this->addResult(0, NULL, 1, -1, "Número de columnas", "Cantidad de columnas diferentes en el archivo y en la base. Adecúe la plantilla");
	}

	function missingColumns(){		
		foreach ($this->columns as $c){
			$e = true;
			foreach ($this->data as $d => $v) if($c->field == $d){
				$e = false;
				break;
			}
			if($e)$this->addResult($c->id, $c->field, 1, -2, "Columna faltante", "Agregue la columna al archivo o quítela del diseño LSMW");
		} 
	}

	function leftoverColumns(){
		foreach ($this->data as $d  => $v) {
			$e = true;
			foreach ($this->columns as $c) if($d == $c->field){
				$e = false;
				break;
			}
			if($e)$this->addResult(0, $d, 1, -3, "Columna sobrante", "Quite la columna del archivo o agréguela al diseño LSMW");
		}
	}

	function emptyRows(){
		$c = 0;
		foreach ($this->data as $d) {
			$c = count($d);
			break;
		}
		for($i = 0; $i < $c; $i++){
			$e = true;
			foreach ($this->data as $d) if($d[$i] != ""){
					$e = false;
					break;
				}
			if($e)$this->addResult(0, NULL, $i + 1, -4, "Fila vacía", "Faltan datos en este archivo, agregue los datos o remueva la fila");
		}
	}

	function formulasFunctions(){
		#foreach ($this->data as $d) for($i = 1; @$d[$i]; $i++)
		foreach($this->columns as $c) for($i = 1; @$this->data[$c->field][$i]; $i++) if(substr($this->data[$c->field][$i], 0, 1) == "=") $this->addResult($c->id, $c->field, $i + 1, -5, "Fórmula / Función", "No se permiten fórmulas o funciones en los campos, sustituya la fórmula o función por su valor en cadena de texto plano");
	}

	#addResult($if, $f, $rw, $ir, $rl, $c)
}
?>