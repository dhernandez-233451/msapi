<?php
include_once 'conex.php';
class CUploadLayout{
	private $muploadly;
	private $id;
	private $title;
	private $fileName;
	private $content;

	function CUploadLayout($data){
		include_once '../models/upload_layouts/mUploadLayout.php';
		$this->muploadly = new MUploadLayout();
		$this->id = $data->uploadLayout->id;
		$this->title = $data->uploadLayout->title;
		$this->fileName = $data->uploadLayout->filename;
		$this->content = $data->uploadLayout->content;
	}

	function control($action){
		switch ($action) {
			case 'upload':
				$this->upload();
				break;
			case 'autocorrections':
				$this->autocorrections();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0057", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function convertStringToArray($dataly){
		$_array = NULL;
		$irows = explode("\n", $dataly); //NO funciona con '\n', deben ser comillas dobles
		$icolumns = explode(",", $irows[0]);
		for($j = 1; @$irows[$j]; $j++){
			$ivalues = explode(",", $irows[$j]);
			$i = 0;
			foreach ($icolumns as $c) $_array[$c][] = $ivalues[$i++];
		}
		return $_array;
	}

	function decodeContet($content){
		$parts = explode( ",", $this->content );
		return base64_decode( $parts[count($parts)-1]);
	}

	function removeFile($fname){
		$r = NULL;
		$dir = "/var/www/html/migrasap/files/";
		try{
			if (file_exists($dir.$fname))unlink($dir.$fname);
		}
		catch(Exception $e){
			$r = $e->getMessage();
		}
		return $r;
	}

	function saveFile($fname, $content){
		$r = NULL;
		$dir = "/var/www/html/migrasap/files/";
		if($fname == $this->title.".csv"){
			try{
				$file = fopen( $dir.$fname, 'wb' );
				fwrite( $file, $this->decodeContet($content));
				fclose( $file );
			}
			catch(Exception $e){
				$r = $e->getMessage();
			}
		}
		else $r = "El nombre del archivo y el LSMW elegido no coinciden";
		
		return $r;
	}

	function readFile($fname){
		$r = NULL;
		$content = NULL;
		$dir = "/var/www/html/migrasap/files/";
		$result = NULL;
		try{
			if(file_exists($dir.$fname)) $content = file_get_contents($dir.$fname);
			else $r = "No se encontró el archivo $fname";
			if($content == NULL)$r = "No se pudo obtener información del archivo $fname";
		}
		catch(Exception $e){
			$r = $e->getMessage();
		}
		$result['r'] = $r;
		$result['content'] = $content;
		return $result;
	}

	function validations($dataAr){
		include_once 'upload_layouts/SystemValidations.php';
		include_once 'upload_layouts/UsersValidations.php';
		$sysval = NULL;
		$ussval = NULL;
		$sysresults = NULL;
		$ussresults = NULL;
		$rlist = NULL;
		$rules = NULL;
		
		$columns = $this->muploadly->select_0($this->id);

		$r1 = $this->muploadly->select_1();
		$r2 = $this->muploadly->select_2($this->id);
		foreach ($r2['res'] as $f) {
			$id_rule = $rule = "NULL";
			foreach ($r1['res'] as $r) if($f->id_rule == $r->id){
					$id_rule = $r->id;
					$rule = $r->name;
					break;
				}
			$rules[] = array('id_field' => $f->id_field, 'field' => $f->field, 'id_rule' => $id_rule, 'rule' => $rule);
		}

		$sysval = new SystemValidations($dataAr, $columns['res']);
		$sysresults = $sysval->results;

		$ussval = new UsersValidations($dataAr, $columns['res'], $rules);
		$ussresults = $ussval->results;

		foreach ($sysresults as $l) $rlist[] = $l;
		foreach ($ussresults as $l) $rlist[] = $l;

		return $rlist;
	}

	function changeValues($dataAr){
		include_once 'upload_layouts/UsersValidations.php';
		$ussval = NULL;

		$columns = $this->muploadly->select_0($this->id);
		$rules = $this->muploadly->select_1($this->id);

		$ussval = new UsersValidations($dataAr, $columns['res'], $rules['res']);
		return $ussval->getChangedValues();
	}

	function saveValues($dataAr){
		$r = NULL;
		$r0 = $this->muploadly->select_0($this->id);
		$r['error'] = $r0['error'];
		if( $r0['error'] == NULL && $r0['res'] != NULL){
			$r['res'] = $r0['res'];
			$r1 = $this->muploadly->delete($this->id);
			$r['error'] = $r1['error'];
			if($r1['error'] == NULL){
				$r['res'] = $r1['res'];
				foreach ($dataAr as $c => $a) foreach ($r0['res'] as $f) if($c == $f->field){
							foreach($a as $v) $r2 = $this->muploadly->insert($f->id, $v);
							break;
						}
				$r['error'] = $r2['error'];
				$r['res'] = $r2['res'];
			}
		}
		return $r;
	}

	function upload(){
		$r = NULL;
		$dataly = NULL;
		$dataAr = NULL;
		$list = NULL;
		$rdb = NULL;

		if($r == NULL) $r = $this->removeFile($this->fileName);
		if($r == NULL) $r = $this->saveFile($this->fileName, $this->content);
		if($r == NULL){
			$dataly = $this->decodeContet($this->content);
			if($dataly == NULL)	$r = "Sin contenido";
		}
		if($r == NULL){
			$dataAr = $this->convertStringToArray($dataly);
			if($dataAr == NULL || count($dataAr) < 2) $r = "Sin datos suficientes";
		}
		if($r == NULL){
			$list = $this->validations($dataAr);
			if($list != NULL) $r = "Corregir obsevaciones";
		} 
		if($r == NULL){
			$rdb = $this->saveValues($dataAr);
			if($rdb['error'] != NULL) $r = $rdb['error'];
		}

		if($r == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"]]);
		else{
			if($list != NULL) print json_encode((object)[error => (object)['code'=> "WMS_0058", 'message' => $r], 
					data => (object)['uploadLayout' => (object)['id' => $this->id, 'title' => $this->title ], 'layoutValidations' => $list]]);
			else print json_encode((object)[error => (object)['code'=> "WMS_0059", 'message' => $r],
					data => (object)['uploadLayout' => null, 'layoutValidations' => null]]);
		}
		$this->muploadly->close_conex();
	}

	function autocorrections(){
		$r = NULL;
		$fcontent = NULL;
		$dataly = NULL;
		$dataAr = NULL;
		$rdb = NULL;

		if($r == NULL){
			$fcontent = $this->readFile($this->fileName);
			$r = $fcontent['r'];
			$dataly = $fcontent['content'];
		}
		if($r == NULL){
			$dataAr = $this->convertStringToArray($dataly);
			if($dataAr == NULL || count($dataAr) < 2) $r = "Sin datos suficientes";
		}
		if($r == NULL){
			$rdb = $this->saveValues($this->changeValues($dataAr));
			if($rdb['error'] != NULL) $r = $rdb['error'];
		}
		if($r == NULL){
			$r = $this->removeFile($this->fileName);
			if($r == NULL) print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"]]);
			else print json_encode((object)[error => (object)['code'=> "", 'message' => "Los datos se actualizaron correctamete. El archivo cargado no se pudo eliminar: ".$r]]);
		}
		else print json_encode((object)[error => (object)['code'=> "WMS_0060", 'message' => $r],
				data => (object)['uploadLayout' => null, 'layoutValidations' => null]]);
		$this->muploadly->close_conex();
	}
}
?>