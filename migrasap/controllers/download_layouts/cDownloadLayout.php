<?php
include_once 'conex.php';
class CDownloadLayout{
	private $mdownloadly;
	private $id;
	private $title;
	private $content;

	function CDownloadLayout($data){
		include_once '../models/download_layouts/mDownloadLayout.php';
		$this->mdownloadly = new MDownloadLayout();
		$this->id = $data->downloadLayout->id;
		$this->title = $data->downloadLayout->title;
		$this->content = $data->downloadLayout->content;
	}

	function control($action){
		switch ($action) {
			case 'download':
				$this->download();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0053", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function assembleTemplate($dataly){
		$cnt = "";
		$values = NULL;
		$column = "";
		$c = -1;
		//Ordena los valores para cada columna y agrega los nombres de columna
		foreach ($dataly as $d) {
			if($column != $d->id_field) {
				$cnt.= $d->field.",";
				$column = $d->id_field;
				$c++;
			}
			$values[$c][] = $d->value;
		}
		$cnt = substr($cnt, 0, -1)."\n";

		//Agrega los valores por fila
		for($i = 0; $i < count($values[0]); $i++ ){
			for($j = 0; $j <= $c; $j++) $cnt.= $values[$j][$i].",";
			$cnt = substr($cnt, 0, -1)."\n";
		}
		$cnt = substr($cnt, 0, -1);

		return $cnt;
	}

	function generateFile($fname, $content){
		$r = NULL;
		$result = NULL;
		$b64file = NULL;
		$dir = "/var/www/html/migrasap/files/";

		try{
			$file = fopen( $dir.$fname, 'wb' );
			fwrite( $file, $content );
			fclose( $file );
			if (file_exists($dir.$fname)){
				$b64file = base64_encode(file_get_contents($dir.$fname));
				unlink($dir.$fname);
			} 
			else $r = "No se pudo generar el archivo $fname";
		}
		catch(Exception $e){
			$r = $e->getMessage();
		}

		$result['error'] = $r;
		$result['file'] = $b64file;
		return $result;
	}

	function download(){
		$extension = "csv";
		$file_result = "";
		$r  = $this->mdownloadly->select($this->id);

		if( $r['error'] == NULL){
			if($r['res'] != NULL){
				$this->title  = $r['res'][0]->lsmw.".".$extension;
				$file_result = $this->generateFile($this->title, $this->assembleTemplate($r['res']));

				if($file_result['error'] == NULL) print json_encode((object)[
					error => (object)['code'=> "", 'message' => "Excelente"], 
					data => (object)['downloadLayout' => (object)['id' => $this->id, 'title' => $this->title, 'content' => $file_result['file'] ]]]);
				else print json_encode((object)[error => (object)['code'=>"WMS_0056", 'message' => $file_result['error']]]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0055", 'message' => "No se encontraron datos ".$this->name]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0054", 'message' => $r['error']]]);

		$this->mdownloadly->close_conex();
	}
}
?>