<?php
class Conex{
    protected $basetype = "mysql",
            $server = "127.0.0.1",            
            $base = "",
            $user = "admin",
            $passwd = "Exca1050",
            $charset = "utf8";
    
    private $_conex, $error;
    
    function Conex($base){
        $this->base = $base;
        try{
            $this->_conex = new PDO($this->basetype.":host=".$this->server.";dbname=".$this->base.";charset=".$this->charset, $this->user, $this->passwd);
            $this->_conex->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex) {
            $this->error = "Error: ".$ex->getMessage();
        }
    }

    function run($sql, $mode){
        $result = array('res' => "", 'error' => "");        
        if($this->error == ""){
            try{
                if($mode == "sel") $result['res'] = $this->_conex->query($sql)->fetchAll(PDO::FETCH_OBJ);
                else $result['res'] = $this->_conex->exec($sql);
            } catch (PDOException $ex) {
                $result['error'] = $ex->getMessage();
            }
        }
        else $result['error'] = $this->error;
        
        #$this->_conex = null;
        return $result;
    }

    function close_conex(){
        $this->_conex = null;
    }
}
?>