<?php
include_once 'conex.php';
class CLocalManage extends Conex{
	private $mlmanage;
	private $dbname;
	private $content;
	private $data;

	function CLocalManage($data){
		include_once '../models/local_manage/mLocalManage.php';
		$this->mlmanage = new MLocalManage();

		$this->dbname = $data->localManage->dbname;
		$this->content = $data->localManage->content;
		$this->data = $data->massiveImport;
	}

	function control($action){
		switch ($action) {
			case 'backup':
				$this->backup();
				break;
			case 'restore':
				$this->restore();
				break;
			case 'clean':
				$this->clean();
				break;
			case 'isempty':
				$this->isempty();
				break;
			case 'import':
				$this->import();
				break;
			case 'transform':
				$this->transform();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0028", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function backup(){
		$r = NULL;
		$b64file = NULL;
		$dir = "/var/www/html/migrasap/files/";
		$fname = $this->dbname."_b".date("Ymd-His").".sql";
		$command = "mysqldump --host=$this->server --user=$this->user --password=$this->passwd $this->dbname > $dir$fname";

		try{
			exec($command);
			if (file_exists($dir.$fname)){
				$b64file = base64_encode(file_get_contents($dir.$fname));
				unlink($dir.$fname);
			} 
			else $r = "No se pudo generar el archivo backup";
			if($b64file == NULL)$r = "No se pudo obtener información del archivo backup";
		}
		catch(Exception $e){
			$r = $e->getMessage();
		}

		if($r == NULL) print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => (object)['localManage' => (object)['dbname' => $fname, 'content' => $b64file]]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0029", 'message' => $r]]);
	}

	function restore(){
		$r = NULL;
		$dir = "/var/www/html/migrasap/files/";
		$fname = $this->dbname."_r".date("Ymd-His").".sql";
		$parts = explode( ',', $this->content );
		$command = "mysql --host=$this->server --user=$this->user --password=$this->passwd $this->dbname < $dir$fname";
		try{
			$file = fopen( $dir.$fname, 'wb' );
			fwrite( $file, base64_decode( $parts[count($parts)-1]));
			fclose( $file );
			if (file_exists($dir.$fname)){
				exec($command, $r);
				unlink($dir.$fname);
			} 
			else $r = "No se pudo generar el archivo restore";
		}
		catch(Exception $e){
			$r = $e->getMessage();
		}

		if($r == NULL) print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0030", 'message' => $r]]);
	}

	function clean(){
		$r = NULL;
		$cmd1 = "mysql --host=$this->server --user=$this->user --password=$this->passwd --init-command=\"SET GLOBAL FOREIGN_KEY_CHECKS = 0;\";";
		$cmd2 = "mysql --host=$this->server --user=$this->user --password=$this->passwd -Nse 'show tables' $this->dbname | while read table; do mysql --host=$this->server --user=$this->user --password=$this->passwd -e \"truncate table \$table\" $this->dbname; done";
		$cmd3 = "mysql --host=$this->server --user=$this->user --password=$this->passwd --init-command=\"SET GLOBAL FOREIGN_KEY_CHECKS = 1;\";";
		try{
			exec($cmd1);
			exec($cmd2);
			exec($cmd3);
		}
		catch(Exception $e){
			$r = $e->getMessage();
		}

		if($r == NULL) print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0031", 'message' => $r]]);
	}

	function isempty(){
		$r = ($this->dbname == "msmirror")? $this->mlmanage->select_0():$this->mlmanage->select_1();
		if( $r['error'] == NULL){
			if($r['res'] == NULL){
				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => (object)['localManage' => null]]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0062", 'message' => "Ya existen datos en ".$this->dbname]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0061", 'message' => $r['error']]]);

		$this->mlmanage->close_conex();
	}

	function import(){
		include_once 'local_manage/PreTreatment.php';
		$treat = new PreTreatment();
		$res = "";
		$c = 0;

		foreach ($this->data as $d) {											//Lista de tablas
			$r0 = $this->mlmanage->select_2($d->idTable);						//Campos de base por tabla
			if($r0['error'] == NULL && $r0['res'] != NULL){
				for($i = 0; $i<count($d->fields); $i++) {						//Campos de tabla
					foreach ($r0['res'] as $f) {
						if($d->fields[$i] == $f->name){							//Campos de tabla en lista y base
							foreach ($d->data as $v) {							//Valores de campo
								$r1 = $this->mlmanage->insert_0($f->id, $treat->rmCharacters($v[$i]));
								if($r1['error'] != NULL){
									$res .= " Falló con ".$v[$i]." en ".$d->table."-".$f->name.". ".$r1['error'].".";
									#break;
								}
								else $c++;
							}
							break;
						}
					}
				}
			}
			else $res .=" No se encontraron datos sobre ".$d->table;
		}

		if($res == "") print json_encode((object)[error => (object)['code'=> "", 'message' => $c." datos procesados"]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0063", 'message' => $res]]);

		$this->mlmanage->close_conex();
	}

	function transform(){
		$res = "";
		$c = 0;

		$r0 = $this->mlmanage->select_3();
		if($r0['error'] == NULL) if($r0['res'] != NULL){
				$r1 = $this->mlmanage->select_0();
				if($r1['error'] == NULL) if($r1['res'] != NULL){
						foreach ($r0['res'] as $m) foreach ($r1['res'] as $d) if($m->id_sap_field == $d->id_sap_field){
									$r2 = $this->mlmanage->insert_1($m->id_lsmw_field, $d->value);
									if($r2['error'] != NULL){
										$res .= " Falló con ".$d->value.", SAP: ".$d->id_sap_field.", LSMW: ".$m->id_lsmw_field.". ".$r2['error'];
										break;
									}
									else $c++;
								}
					}
					else $res .= " No se encontraron datos SAP.";
				else $res .= " ".$r1['error'];
			}
			else $res .= " No se encontaron datos de mapeo.";
		else $res .= " ".$r0['error'];

		if($res == "") print json_encode((object)[error => (object)['code'=> "", 'message' => $c." datos procesados"]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0064", 'message' => $res]]);

		$this->mlmanage->close_conex();
	}
}
?>