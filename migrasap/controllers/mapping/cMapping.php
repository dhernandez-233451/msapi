<?php
class CMapping{
	private $mmapping;
	private $id_lsmw;
	private $mapping;

	function CMapping($data){
		include_once '../models/mapping/mMapping.php';
		$this->mmapping = new MMapping();

		$this->id_lsmw = $data->id_lsmw;
		$this->mapping = $data->mapping;
	}

	function control($action){
		switch ($action) {
			case 'find':
				$this->find();
				break;
			case 'new':
				$this->new();
				break;
			case 'edit':
				$this->edit();
				break;
			case 'delete':
				$this->delete();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0044", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function find(){
		$r0  = $this->mmapping->select_0($this->id_lsmw);
		$r1  = $this->mmapping->select_1();
		$r = NULL;
		$found = false;

		if( $r0['error'] == NULL && $r1['error'] == NULL ){
			if($r0['res'] != NULL){
				if($r1['res'] != NULL){
					foreach ($r0['res'] as $l){
						$found = false;
						foreach ($r1['res'] as $t) if($l->mapping_sap == $t->id_sap_field){
								$found = true;
								break;
							}
						if($found){
							$r[] = ['id_lsmw' => $l->id_lsmw, 'lsmw' => $l->lsmw, 'id_lsmw_field' => $l->id_lsmw_field, 'lsmw_field' => $l->lsmw_field, 'id_sap_table' => $t->id_sap_table, 'sap_table' => $t->sap_table, 'id_sap_field' => $t->id_sap_field, 'sap_field' => $t->sap_field];
						}
						else{
							$r[] = ['id_lsmw' => $l->id_lsmw, 'lsmw' => $l->lsmw, 'id_lsmw_field' => $l->id_lsmw_field, 'lsmw_field' => $l->lsmw_field, 'id_sap_table' => null, 'sap_table' => null, 'id_sap_field' => null, 'sap_field' => null];
						}
					}
					print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => (object)['mapping' => $r]]);
				}
				else print json_encode((object)[error => (object)['code'=>"WMS_0047", 'message' => "No se encontraron datos de tablas ".$this->id_lsmw]]);
				
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0046", 'message' => "No se encontraron datos de LSMW ".$this->id_lsmw]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0045", 'message' => $r0['error']]]);

		$this->mmapping->close_conex();
	}

	/*function new(){
		$r  = $this->mmapping->insert($this->id_lsmw, $this->field);
		$this->mmapping->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0041", 'message' => $r['error']]]);
	}*/

	function edit(){		
		#print_r($this->mapping);
		foreach ($this->mapping as $m) {
			#print_r($m->id_lsmw_field);
			$r0  = $this->mmapping->delete($m->id_lsmw_field);
			$r1  = $this->mmapping->insert($m->id_lsmw_field, $m->id_sap_field);
		}
		$this->mmapping->close_conex();

		if( $r0['error'] == NULL && $r1['error'] == NULL )print json_encode((object)[error => (object)['code'=> "", 'message' => $r0['res']."_".$r1['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0048", 'message' => $r0['error']."_".$r1['error']]]);
	}

	/*function delete(){
		$r  = $this->mmapping->delete($this->id);
		$this->mmapping->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0043", 'message' => $r['error']]]);
	}*/
}
?>