<?php
class CUser{
	private $muser;
	private $id;
	private $id_profile;
	private $name;
	private $user;
	private $passwd;

	function CUser($data){
		include_once '../models/users/mUser.php';
		$this->muser = new MUser();

		$this->id = $data->user->id;
		$this->id_profile = $data->user->id_profile;
		$this->name = $data->user->name;
		$this->user = $data->user->user;
		$this->passwd = $data->user->passwd;
	}

	function control($action){
		switch ($action) {
			case 'find':
				$this->find();
				break;
			case 'new':
				$this->new();
				break;
			case 'edit':
				$this->edit();
				break;
			case 'delete':
				$this->delete();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0016", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function find(){
		$r  = $this->muser->select($this->id, $this->user);
		$r0  = $this->muser->select_0();

		if( $r['error'] == NULL && $r0['error'] == NULL ){
			if( $r['res'] != NULL && $r0['res'] != NULL ){
				/*foreach ($r['res'] as $d) $result['users'][] = ['id' => $d->id, 'name' => $d->name];

				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => $result]);*/
				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => (object)['profiles' => $r0['res'], 'users' => $r['res']]]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0018", 'message' => "No se encontraron datos ".$this->name]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0017", 'message' => $r['error']]]);

		$this->muser->close_conex();
	}

	function new(){
		$r  = $this->muser->insert($this->id_profile, $this->name, $this->user, $this->passwd);
		$this->muser->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0019", 'message' => $r['error']]]);
	}

	function edit(){
		$r  = ( $this->passwd == md5(null) ) ? $this->muser->update($this->id, $this->id_profile, $this->name) : $this->muser->update_0($this->id, $this->id_profile, $this->name, $this->passwd);
		$this->muser->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0020", 'message' => $r['error']]]);
	}

	function delete(){
		$r  = $this->muser->delete($this->id);
		$this->muser->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0021", 'message' => $r['error']]]);
	}
}
?>