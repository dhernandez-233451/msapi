<?php
header('Content-Type: text/html; charset=UTF-8');
class main{
	private $data;

	function main(){
		$post= file_get_contents('php://input');
		#$decode = json_decode(utf8_encode($post));
        $decode = json_decode($post);
        $this->data = $decode->data;
        $this->control($decode->auth);
	}

	function control($auth){
        switch ($auth->module){
            case 'login':
                $this->login($auth->action);
                break;
            case 'profiles':
                $this->profiles($auth->action);
                break;
            case 'modules':
                $this->modules($auth->action);
                break;
            case 'users':
                $this->users($auth->action);
                break;
            case 'sapconn':
                $this->sapconn($auth->action);
                break;
            case 'localmanage':
                $this->localmanage($auth->action);
                break;
            case 'saptables':
                $this->saptables($auth->action);
                break;
            case 'sapfields':
                $this->sapfields($auth->action);
                break;
            case 'lsmws':
                $this->lsmws($auth->action);
                break;
            case 'lsmwfields':
                $this->lsmwfields($auth->action);
                break;
            case 'mapping':
                $this->mapping($auth->action);
                break;
            case 'rules':
                $this->rules($auth->action);
                break;
            case 'downloadlayouts':
                $this->downloadlayouts($auth->action);
                break;
            case 'uploadlayouts':
                $this->uploadlayouts($auth->action);
                break;
            default :
                print json_encode((object)[error => (object)['code'=>"WMS_0001", 'message' => "No se encontró el módulo ".$auth->module]]);
                break;
        }
    }

    function login($action){
        include_once 'login/cLogin.php';
        $clogin = new CLogin($this->data);
        $clogin->control($action);
    }

    function profiles($action){
        include_once 'profiles/cProfile.php';
        $cprofile = new CProfile($this->data);
        $cprofile->control($action);
    }

    function modules($action){
        include_once 'profiles/cModule.php';
        $cmodule = new CModule($this->data);
        $cmodule->control($action);
    }

    function users($action){
        include_once 'users/cUser.php';
        $cuser = new CUser($this->data);
        $cuser->control($action);
    }

    function sapconn($action){
        include_once 'sap_conn/cSapConn.php';
        $csapconn = new CSapConn($this->data);
        $csapconn->control($action);
    }

    function localmanage($action){
        include_once 'local_manage/cLocalManage.php';
        $clmanage = new CLocalManage($this->data);
        $clmanage->control($action);
    }

    function saptables($action){
        include_once 'sap_tables/cSapTable.php';
        $csaptable = new CSapTable($this->data);
        $csaptable->control($action);
    }

    function sapfields($action){
        include_once 'sap_fields/cSapField.php';
        $csapfield = new CSapField($this->data);
        $csapfield->control($action);
    }

    function lsmws($action){
        include_once 'lsmws/cLsmw.php';
        $clsmw = new CLsmw($this->data);
        $clsmw->control($action);
    }

    function lsmwfields($action){
        include_once 'lsmw_fields/cLsmwField.php';
        $clsmwfield = new CLsmwField($this->data);
        $clsmwfield->control($action);
    }

    function mapping($action){
        include_once 'mapping/cMapping.php';
        $cmapping = new CMapping($this->data);
        $cmapping->control($action);
    }

    function rules($action){
        include_once 'rules/cRules.php';
        $crules = new CRules($this->data);
        $crules->control($action);
    }

    function downloadlayouts($action){
        include_once 'download_layouts/cDownloadLayout.php';
        $cdownloadly= new CDownloadLayout($this->data);
        $cdownloadly->control($action);
    }

    function uploadlayouts($action){
        include_once 'upload_layouts/cUploadLayout.php';
        $cuploadly= new CUploadLayout($this->data);
        $cuploadly->control($action);
    }
}
$main = new main();
?>