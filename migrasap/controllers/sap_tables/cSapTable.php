<?php
class CSapTable{
	private $msaptable;
	private $id;
	private $name;

	function CSapTable($data){
		include_once '../models/sap_tables/mSapTable.php';
		$this->msaptable = new MSapTable();

		$this->id = $data->sapTable->id;
		$this->name = $data->sapTable->name;
	}

	function control($action){
		switch ($action) {
			case 'find':
				$this->find();
				break;
			case 'new':
				$this->new();
				break;
			case 'edit':
				$this->edit();
				break;
			case 'delete':
				$this->delete();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0032", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function find(){
		$r  = $this->msaptable->select($this->id, $this->name);

		if( $r['error'] == NULL){
			if($r['res'] != NULL){
				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => (object)['sapTables' => $r['res']]]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0034", 'message' => "No se encontraron datos ".$this->name]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0033", 'message' => $r['error']]]);

		$this->msaptable->close_conex();
	}

	function new(){
		$r  = $this->msaptable->insert($this->name);
		$this->msaptable->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0035", 'message' => $r['error']]]);
	}

	function edit(){
		$r  = $this->msaptable->update($this->id, $this->name);
		$this->msaptable->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0036", 'message' => $r['error']]]);
	}

	function delete(){
		$r  = $this->msaptable->delete($this->id);
		$this->msaptable->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0037", 'message' => $r['error']]]);
	}
}
?>