<?php
class CSapConn{
	private $msapconn;
	private $id;
    private $name;
    private $host;
    private $sysnr;
    private $client;
    private $user;
    private $passwd;
    private $lang;
    private $saprouter;
    private $function;

	function CSapConn($data){
		include_once '../models/sap_conn/mSapConn.php';
		$this->msapconn = new MSapConn();

		$this->id = $data->sapConn->id;
		$this->name = $data->sapConn->name;
		$this->host = $data->sapConn->host;
		$this->sysnr = $data->sapConn->sysnr;
		$this->client = $data->sapConn->client;
		$this->user = $data->sapConn->user;
		$this->passwd = $data->sapConn->passwd;
		$this->lang = $data->sapConn->lang;
		$this->saprouter = $data->sapConn->saprouter;
		$this->function = $data->sapConn->function;
	}

	function control($action){
		switch ($action) {
			case 'find':
				$this->find();
				break;
			case 'new':
				$this->new();
				break;
			case 'edit':
				$this->edit();
				break;
			case 'delete':
				$this->delete();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0022", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function find(){
		$r  = $this->msapconn->select($this->id, $this->host);

		if( $r['error'] == NULL ){
			if( $r['res'] != NULL ){
				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => (object)['sapConns' => $r['res']]]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0024", 'message' => "No se encontraron datos ".$this->name]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0023", 'message' => $r['error']]]);

		$this->msapconn->close_conex();
	}

	function new(){
		$r  = $this->msapconn->insert($this->name, $this->host, $this->sysnr, $this->client, $this->user, $this->passwd, $this->lang, $this->saprouter, $this->function);
		$this->msapconn->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0025", 'message' => $r['error']]]);
	}

	function edit(){
		$r  = ( $this->passwd == md5(null) ) ? $this->msapconn->update($this->id, $this->name, $this->host, $this->sysnr, $this->client, $this->user, $this->lang, $this->saprouter, $this->function) : $this->msapconn->update_0($this->id, $this->name, $this->host, $this->sysnr, $this->client, $this->user, $this->passwd, $this->lang, $this->saprouter, $this->function);
		$this->msapconn->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0026", 'message' => $r['error']]]);
	}

	function delete(){
		$r  = $this->msapconn->delete($this->id);
		$this->msapconn->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0027", 'message' => $r['error']]]);
	}
}
?>