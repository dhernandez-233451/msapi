<?php
class CRules{
	private $mrules;
	private $id_lsmw_field;
	private $rules;

	function CRules($data){
		include_once '../models/rules/mRules.php';
		$this->mrules = new MRules();

		$this->id_lsmw_field = $data->rules[0]->id_lsmw_field;
		$this->rules = $data->rules;
	}

	function control($action){
		switch ($action) {
			case 'find':
				$this->find();
				break;
			case 'new':
				$this->new();
				break;
			case 'edit':
				$this->edit();
				break;
			case 'delete':
				$this->delete();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0049", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function find(){
		$res = NULL;
		$r0  = $this->mrules->select_0();
		$r1  = $this->mrules->select_1($this->id_lsmw_field);

		if( $r0['error'] == NULL && $r1['error'] == NULL ){
			if($r0['res'] != NULL){
				foreach ($r0['res'] as $r) {
					$active = 'false';
					foreach ($r1['res'] as $rl) if($rl->id_rule == $r->id){
							$active = 'true';
							break;
						}
					$res[] = array('id_lsmw_field' => $this->id_lsmw_field, 'id_rule' => $r->id, 'rule' => $r->name, 'active' => $active);
				}
				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => (object)['rules' => $res]]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0051", 'message' => "No se encontraron datos ".$this->name]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0050", 'message' => $r0['error'].$r1['error']]]);

		$this->mrules->close_conex();
	}

	/*function new(){
		$r  = $this->mrules->insert($this->id_lsmw, $this->field);
		$this->mrules->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0041", 'message' => $r['error']]]);
	}*/

	function edit(){
		$r0  = $this->mrules->delete($this->id_lsmw_field);
		foreach ( $this->rules as $rs ) if( $rs->active == TRUE ) $r1 = $this->mrules->insert( $rs->id_rule, $this->id_lsmw_field );
		$this->mrules->close_conex();

		if( $r0['error'] == NULL && $r1['error'] == NULL )print json_encode((object)[error => (object)['code'=> "", 'message' => $r0['res']."_".$r1['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0052", 'message' => $r0['error']."_".$r1['error']]]);
	}

	/*function delete(){
		$r  = $this->mrules->delete($this->id);
		$this->mrules->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0043", 'message' => $r['error']]]);
	}*/
}
?>