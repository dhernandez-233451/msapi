<?php
class CProfile{
	private $mprofile;
	private $id;
	private $name;

	function CProfile($data){
		include_once '../models/profiles/mProfile.php';
		$this->mprofile = new MProfile();

		$this->id = $data->profile->id;
		$this->name = $data->profile->name;
	}

	function control($action){
		switch ($action) {
			case 'find':
				$this->find();
				break;
			case 'new':
				$this->new();
				break;
			case 'edit':
				$this->edit();
				break;
			case 'delete':
				$this->delete();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0005", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function find(){
		$r  = $this->mprofile->select($this->id, $this->name);

		if( $r['error'] == NULL){
			if($r['res'] != NULL){
				foreach ($r['res'] as $d) $result['profiles'][] = ['id' => $d->id, 'name' => $d->name];

				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => $result]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0007", 'message' => "No se encontraron datos ".$this->name]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0006", 'message' => $r['error']]]);

		$this->mprofile->close_conex();
	}

	function new(){
		$r  = $this->mprofile->insert($this->name);
		$this->mprofile->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0008", 'message' => $r['error']]]);
	}

	function edit(){
		$r  = $this->mprofile->update($this->id, $this->name);
		$this->mprofile->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0009", 'message' => $r['error']]]);
	}

	function delete(){
		$r  = $this->mprofile->delete($this->id);
		$this->mprofile->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0010", 'message' => $r['error']]]);
	}
}
?>