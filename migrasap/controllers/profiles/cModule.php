<?php
class CModule{
	private $mmodule;
	private $id;
	private $name;
	private $modules;

	function CModule($data){
		include_once '../models/profiles/mModule.php';
		$this->mmodule = new MModule();

		$this->id = $data->profile->id;
		$this->name = $data->profile->name;
		$this->modules = $data->modules;
	}

	function control($action){
		switch ($action) {
			case 'find':
				$this->find();
				break;
			case 'edit':
				$this->edit();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0011", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function find(){
		$r  = $this->mmodule->select($this->id);

		if( $r['error'] == NULL){
			if($r['res'] != NULL){
				foreach ($r['res'] as $d) $result['modules'][] = ['id' => $d->id, 'name' => $d->name, 'active' => ($d->active == 1)? 'true' : 'false'];

				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => $result]);
				#print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
				#							data => (object)['modules' => $r['res']]]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0013", 'message' => "No se encontraron datos ".$this->name]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0012", 'message' => $r['error']]]);

		$this->mmodule->close_conex();
	}

	function edit(){
		$r  = $this->mmodule->delete($this->id);

		if( $r['error'] == NULL){
			foreach ($this->modules as $m) if ($m->active == TRUE) {
				$r1  = $this->mmodule->insert($this->id, $m->id);
				if($r1['error'] != NULL) break;
			}
			if ($r1['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r1['res']]]);
			else print json_encode((object)[error => (object)['code'=>"WMS_0015", 'message' => $r1['error']]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0014", 'message' => $r['error']]]);

		$this->mmodule->close_conex();
	}
}
?>