<?php
class CSapField{
	private $msapfield;
	private $id;
	private $id_sap_table;
	private $name;

	function CSapField($data){
		include_once '../models/sap_fields/mSapField.php';
		$this->msapfield = new MSapField();

		$this->id = $data->sapField->id_sap_table;
		$this->id_sap_table = $data->sapField->id_sap_table;
		$this->name = $data->sapField->name;
	}

	function control($action){
		switch ($action) {
			case 'find':
				$this->find();
				break;
			case 'new':
				$this->new();
				break;
			case 'edit':
				$this->edit();
				break;
			case 'delete':
				$this->delete();
				break;
			
			default:
				print json_encode((object)[error => (object)['code'=>"WMS_0038", 'message' => "No se encontró la indicación ".$action]]);
				break;
		}
	}

	function find(){
		$r  = $this->msapfield->select($this->id_sap_table, $this->name);

		if( $r['error'] == NULL){
			if($r['res'] != NULL){
				print json_encode((object)[error => (object)['code'=> "", 'message' => "Excelente"],
											data => (object)['sapFields' => $r['res']]]);
			}
			else print json_encode((object)[error => (object)['code'=>"WMS_0040", 'message' => "No se encontraron datos ".$this->name]]);
		}
		else print json_encode((object)[error => (object)['code'=>"WMS_0039", 'message' => $r['error']]]);

		$this->msapfield->close_conex();
	}

	function new(){
		$r  = $this->msapfield->insert($this->id_sap_table, $this->name);
		$this->msapfield->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0041", 'message' => $r['error']]]);
	}

	function edit(){
		$r  = $this->msapfield->update($this->id, $this->name);
		$this->msapfield->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0042", 'message' => $r['error']]]);
	}

	function delete(){
		$r  = $this->msapfield->delete($this->id);
		$this->msapfield->close_conex();

		if( $r['error'] == NULL)print json_encode((object)[error => (object)['code'=> "", 'message' => $r['res']]]);
		else print json_encode((object)[error => (object)['code'=>"WMS_0043", 'message' => $r['error']]]);
	}
}
?>